<?php
declare(strict_types=1);

namespace App\Infrastructure\DrivenAdapter\Storage;

use App\Domain\Exception\MovieNotFound;
use App\Domain\Exception\ServiceNotResponding;
use App\Domain\Ports\MovieProvider;
use App\Domain\ReadModel\Collection\MovieSummaryCollection;
use App\Domain\ReadModel\Collection\PeopleCollection;
use App\Domain\ReadModel\Movie;
use App\Domain\ReadModel\MovieSummary;
use App\Domain\ReadModel\Pagination\PaginatedMovieSummary;
use App\Domain\ReadModel\People;
use App\Domain\ValueObject\PaginationParameters;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Psr\Log\LoggerInterface;

final readonly class DbalMovieProvider implements MovieProvider
{
    public function __construct(private Connection $connection, private LoggerInterface $logger) {}

    /** @throws MovieNotFound */
    public function get(int $id): Movie
    {
        try {
            $movieData = $this->connection->createQueryBuilder()
                ->select(
                    'm.id as mid', 'm.title', 'm.duration',
                    'p.id as pid', 'p.firstname', 'p.lastname', 'p.date_of_birth', 'p.nationality', 'mhp.role', 'mhp.significance',
                    't.id as tid', 't.name')
                ->from('movie', 'm')
                ->leftJoin('m', 'movie_has_people', 'mhp', 'm.id = mhp.movie_id')
                ->leftJoin('mhp', 'people', 'p', 'mhp.people_id = p.id')
                ->leftJoin('m', 'movie_has_type', 'mht', 'm.id = mht.movie_id')
                ->leftJoin('mht', 'type', 't', 'mht.Type_id = t.id')
                ->where('m.id = :id')
                ->setParameter('id', $id)
                ->executeQuery()
                ->fetchAllAssociative();

            if ($movieData === []) {
                throw new MovieNotFound();
            }

            $peopleCollection = new PeopleCollection();
            $types = [];
            foreach ($movieData as $datum) {
                if (isset($datum['pid'])) {
                    $peopleCollection->addPeople(new People(
                        $datum['pid'],
                        $datum['firstname'],
                        $datum['lastname'],
                        $datum['date_of_birth'],
                        $datum['nationality'],
                        $datum['role'],
                        $datum['significance'] ?? null
                    ));
                }
                if (isset($datum['tid']) && isset($types[$datum['tid']]) === false) {
                    $types[$datum['tid']] = $datum['name'];
                }
            }

            $types = array_values($types);

            return new Movie(
                $movieData[0]['mid'],
                $movieData[0]['title'],
                $movieData[0]['duration'],
                $peopleCollection,
                $types
            );
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());

            throw new ServiceNotResponding('MySQL', self::class, $e);
        }
    }

    public function paginateMovieSummaries(PaginationParameters $paginationParameters): PaginatedMovieSummary
    {
        $movieSummaryListData = $this->getMovieSummaryListData(
            $paginationParameters->itemsPerPage,
            $paginationParameters->page
        );
        $movieSummaryCollection = $this->buildMovieSummaryCollectionFromData($movieSummaryListData);
        $totalMovieCount = $this->getTotalMovieCount();

        return new PaginatedMovieSummary(
            $paginationParameters->itemsPerPage,
            $totalMovieCount ?? $movieSummaryCollection->count(),
            $paginationParameters->page,
            $movieSummaryCollection
        );
    }

    private function getMovieSummaryListData(int $itemsPerPage, int $page): array
    {
        try {
            return $this->connection->createQueryBuilder()
                ->select(
                    'm.id',
                    'm.title',
                    'm.duration',
                    'GROUP_CONCAT(DISTINCT mhp.people_id) as people_ids',
                    'GROUP_CONCAT(DISTINCT t.name) as type_names')
                ->from('movie', 'm')
                ->leftJoin('m', 'movie_has_people', 'mhp', 'm.id = mhp.movie_id')
                ->leftJoin('m', 'movie_has_type', 'mht', 'm.id = mht.movie_id')
                ->leftJoin('mht', 'type', 't', 'mht.Type_id = t.id')
                ->setFirstResult($itemsPerPage * ($page - 1))
                ->setMaxResults($itemsPerPage)
                ->groupBy('m.id')
                ->executeQuery()
                ->fetchAllAssociative();
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());

            throw new ServiceNotResponding('MySQL', self::class, $e);
        }
    }

    private function buildMovieSummaryCollectionFromData(array $movieListData): MovieSummaryCollection
    {
        $movieSummaryCollection = new MovieSummaryCollection();
        foreach ($movieListData as $movieData) {
            if ($movieData['people_ids'] !== null) {
                $peopleIds = explode(',', $movieData['people_ids']);
                $peopleIds = array_map(fn(string $id) => (int) $id, $peopleIds);
            }

            if ($movieData['type_names'] !== null) {
                $movieTypeNames = explode(',', $movieData['type_names']);
            }

            $movieSummaryCollection->addMovieSummary(new MovieSummary(
                $movieData['id'],
                $movieData['title'],
                $movieData['duration'],
                $peopleIds ?? [],
                $movieTypeNames ?? [],
            ));
        }

        return $movieSummaryCollection;
    }

    private function getTotalMovieCount(): ?int
    {
        try {
            $result = $this->connection->createQueryBuilder()
                ->select('COUNT(id)')
                ->from('movie')
                ->executeQuery()
                ->fetchOne();

            if ($result === false) {
                $this->logger->warning('Could not count movies');

                return null;
            }

            return $result;
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());

            throw new ServiceNotResponding('MySQL', self::class, $e);
        }
    }
}
