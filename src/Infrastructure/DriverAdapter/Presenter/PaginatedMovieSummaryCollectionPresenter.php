<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\Presenter;

use App\Domain\Exception\ServiceNotResponding;
use App\Domain\ReadModel\Pagination\PaginatedMovieSummary;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

final readonly class PaginatedMovieSummaryCollectionPresenter
{
    public function __construct(private RouterInterface $router) {}

    public function expose(PaginatedMovieSummary $paginatedMovieSummary): array
    {
        try {
        $movies = [];

        foreach ($paginatedMovieSummary->collection->listAll() as $movie) {
            $peopleLinks = [];
            foreach ($movie->peopleIds as $peopleId) {
                $peopleLinks[] = $this->router->generate('get_people', ['id' => $peopleId], UrlGeneratorInterface::ABSOLUTE_URL);
            }
            $movies[] = [
                'id' => $movie->id,
                'completeVersion' => $this->router->generate('get_movie', ['id' => $movie->id], UrlGeneratorInterface::ABSOLUTE_URL),
                'title' => $movie->title,
                'duration' => $movie->duration,
                'types' => $movie->movieTypeNames,
                'people' => $peopleLinks,
            ];

        }

        return [
            'itemsPerPage' => $paginatedMovieSummary->itemsPerPage,
            'totalItems' => $paginatedMovieSummary->totalItems,
            'currentPage' => $paginatedMovieSummary->currentPage,
            'totalPages' => $paginatedMovieSummary->totalPages(),
            'prevPage' => $this->generatePageUrl($paginatedMovieSummary->prevPage(), $paginatedMovieSummary->itemsPerPage),
            'nextPage' => $this->generatePageUrl($paginatedMovieSummary->nextPage(), $paginatedMovieSummary->itemsPerPage),
            'items' => $movies,
        ];
        } catch (InvalidParameterException|MissingMandatoryParametersException|RouteNotFoundException $e) {
            throw new ServiceNotResponding('Router', self::class, $e);
        }
    }

    /**
     * @throws RouteNotFoundException
     * @throws InvalidParameterException
     * @throws MissingMandatoryParametersException
     */
    private function generatePageUrl(?int $pageNumber, int $itemsPerPage): ?string
    {
        if ($pageNumber === null) {
            return null;
        }

        return $this->router->generate(
            'list_movies',
            ['page' => $pageNumber, 'itemsPerPage' => $itemsPerPage],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }
}
