<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\Presenter;

use App\Domain\ReadModel\Movie;

final class MoviePresenter
{
    public function expose(Movie $movie): array
    {
        $peopleByRole = [];
        foreach ($movie->peopleCollection->listAll() as $people) {
            $peopleByRole[$people->role][] = [
                'id' => $people->id,
                'firstname' => $people->firstname,
                'lastname' => $people->lastname,
                'dateOfBirth' => $people->dateOfBirth,
                'nationality' => $people->nationality,
                'significance' => $people->significance,
            ];
        }

        return [
            'id' => $movie->id,
            'title' => $movie->title,
            'duration' => $movie->duration,
            'people' => $peopleByRole,
            'types' => $movie->movieTypeNames,
        ];
    }
}
