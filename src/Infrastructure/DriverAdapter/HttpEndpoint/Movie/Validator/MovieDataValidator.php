<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\Movie\Validator;

use App\Domain\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final readonly class MovieDataValidator
{
    public function __construct(private ValidatorInterface $validator) {}

    /** @throws InvalidArgumentException */
    public function validateData(array $inputData): void
    {
        $constraints = new Collection([
            'allowExtraFields' => true,
            'fields' => [
                'title' => new NotBlank(),
                'duration' => new Type('int'),
            ],
        ]);

        $violationList = $this->validator->validate($inputData, $constraints);

        if ($violationList->count() > 0) {
            $errors = [];
            foreach ($violationList as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }

            throw new InvalidArgumentException($errors);
        }
    }
}
