<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint;

use App\Infrastructure\DriverAdapter\HttpEndpoint\Exception\UnauthorizedException;
use Symfony\Component\HttpFoundation\Request;

final readonly class AuthorizationChecker
{
    public function __construct(private string $apiKey) {}

    /** @throws UnauthorizedException */
    public function checkUserAuthorization(Request $request): void
    {
        // This is just a placeholder since I don't have time to implement a whole user system
        if (
            $request->headers->has('X-API-Key') === false
            || $request->headers->get('X-API-Key') !== $this->apiKey
        ) {
            throw new UnauthorizedException();
        }
    }
}
