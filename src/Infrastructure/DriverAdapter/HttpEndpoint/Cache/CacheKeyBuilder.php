<?php
declare(strict_types=1);

namespace App\Infrastructure\DriverAdapter\HttpEndpoint\Cache;

use App\Domain\ValueObject\PaginationParameters;

final readonly class CacheKeyBuilder
{
    public function forPaginatedList(PaginationParameters $paginationParameters): string
    {
        return "listMovies-$paginationParameters->page-$paginationParameters->itemsPerPage";
    }

    public function forMovie(int $id): string
    {
        return "movie-$id";
    }
}
