<?php
declare(strict_types=1);

namespace App\Domain\ValueObject;

use App\Domain\Exception\InvalidArgumentException;

final readonly class PaginationParameters
{
    private const DEFAULT_PAGE = 1;
    private const DEFAULT_MOVIES_PER_PAGE = 50;

    /** @throws InvalidArgumentException */
    public function __construct(
        public int $page = self::DEFAULT_PAGE,
        public int $itemsPerPage = self::DEFAULT_MOVIES_PER_PAGE,
    ) {
        $errors = [];
        if ($this->page < 1) {
            $errors['[page]'] = 'Should be a number greater than 1';
        }

        if ($this->itemsPerPage < 1) {
            $errors['[itemsPerPage]'] = 'Should be a number greater than 1';
        }

        if ($errors !== []) {
            throw new InvalidArgumentException($errors);
        }
    }
}
