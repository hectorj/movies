<?php
declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\Ports\MovieRepository;

final readonly class CreateMovieHandler
{
    public function __construct(private MovieRepository $movieRepository) {}

    public function execute(CreateMovie $command): int
    {
        return $this->movieRepository->add($command);
    }
}
