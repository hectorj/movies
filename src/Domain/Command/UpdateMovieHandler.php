<?php
declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\Exception\MovieNotFound;
use App\Domain\Ports\MovieProvider;
use App\Domain\Ports\MovieRepository;
use App\Domain\ReadModel\Movie;

final readonly class UpdateMovieHandler
{
    public function __construct(private MovieRepository $movieRepository, private MovieProvider $movieProvider) {}

    /** @throws MovieNotFound */
    public function execute(UpdateMovie $command): Movie
    {
        $this->movieRepository->update($command);

        return $this->movieProvider->get($command->id);
    }
}
