<?php
declare(strict_types=1);

namespace App\Domain\Command;

use App\Domain\Exception\InvalidArgumentException;

final readonly class CreateMovie
{
    /** @throws InvalidArgumentException */
    public function __construct(
        public string $title,
        public int $duration,
    ) {
        $errors = [];
        if (\mb_strlen($this->title) > 255) {
            $errors['[title]'] = 'Title is too long. It cannot be greater than 255 characters long.';
        }

        if ($this->duration < 1) {
            $errors['[duration]'] = 'Duration cannot be lesser than 1.';
        }

        if ($errors !== []) {
            throw new InvalidArgumentException($errors);
        }
    }
}
