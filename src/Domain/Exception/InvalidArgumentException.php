<?php
declare(strict_types=1);

namespace App\Domain\Exception;

final class InvalidArgumentException extends \InvalidArgumentException
{
    public function __construct(public readonly array $errors)
    {
        parent::__construct('Invalid argument exception');
    }
}
