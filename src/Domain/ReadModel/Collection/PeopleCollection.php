<?php
declare(strict_types=1);

namespace App\Domain\ReadModel\Collection;

use App\Domain\ReadModel\People;

final class PeopleCollection
{
    /** @param People[] $people */
    public function __construct(
        private array $people = []
    ) {}

    /** @return People[] */
    public function listAll(): array
    {
        return $this->people;
    }

    public function addPeople(People $people): void
    {
        $this->people[$people->id] = $people;
    }
}
