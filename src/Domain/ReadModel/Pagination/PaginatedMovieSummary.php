<?php
declare(strict_types=1);

namespace App\Domain\ReadModel\Pagination;

use App\Domain\ReadModel\Collection\MovieSummaryCollection;

final readonly class PaginatedMovieSummary
{
    public function __construct(
        public int $itemsPerPage,
        public int $totalItems,
        public int $currentPage,
        public MovieSummaryCollection $collection
    ) {}

    public function totalPages(): int
    {
        return (int)\ceil($this->totalItems / $this->itemsPerPage);
    }

    public function prevPage(): ?int
    {
        return $this->currentPage > 1 ? $this->currentPage - 1 : null;
    }

    public function nextPage(): ?int
    {
        return $this->currentPage < $this->totalPages() ? $this->currentPage + 1 : null;
    }
}
