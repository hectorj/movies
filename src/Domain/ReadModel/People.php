<?php
declare(strict_types=1);

namespace App\Domain\ReadModel;

final readonly class People
{
    public function __construct(
        public int $id,
        public string $firstname,
        public string $lastname,
        public string $dateOfBirth,
        public string $nationality,
        public string $role,
        public ?string $significance
    ) {}
}
