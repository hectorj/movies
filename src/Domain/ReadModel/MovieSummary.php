<?php
declare(strict_types=1);

namespace App\Domain\ReadModel;

final readonly class MovieSummary
{
    /**
     * @param int[] $peopleIds
     * @param string[] $movieTypeNames
     */
    public function __construct(
        public int $id,
        public string $title,
        public int $duration,
        public array $peopleIds,
        public array $movieTypeNames,
    ) {}
}
