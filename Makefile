# Executables (local)
DOCKER_COMP = docker compose

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec php

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer
SYMFONY  = $(PHP_CONT) bin/console
PHPUNIT  = $(PHP_CONT) bin/phpunit

# Doctrine migrations commands
MIGRATIONS_GENERATE = doctrine:migrations:generate
MIGRATIONS_MIGRATE = doctrine:migrations:migrate --no-interaction
DATABASE_CREATE_TEST = doctrine:database:create --env=test --no-interaction
MIGRATIONS_MIGRATE_TEST = doctrine:migrations:execute DoctrineMigrations\\Version20230107124337 --env=test --no-interaction

# Misc
.DEFAULT_GOAL = help
.PHONY        : init help build up start down logs sh composer vendor sf cc mm test-setup-db test-mm test

## —— 🎵 🐳 The Symfony Docker Makefile 🐳 🎵 ——————————————————————————————————
init: start vendor cc ## Build and start the containers, install vendors

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9\./_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(DOCKER_COMP) build --pull --no-cache

up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

start: build up ## Build and start the containers

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

sh: ## Connect to the PHP FPM container
	@$(PHP_CONT) sh

## —— Composer 🧙 ——————————————————————————————————————————————————————————————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	@$(eval c ?=)
	@$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor: c=install --prefer-dist --no-progress --no-scripts --no-interaction
vendor: composer

## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make sf c=about
	@$(eval c ?=)
	@$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: sf

## —— Doctrine migrations ——————————————————————————————————————————————————————
mm: c=$(MIGRATIONS_MIGRATE) ## Execute Doctrine migrations
mm: sf

## —— Tests ————————————————————————————————————————————————————————————————————
test-setup-db: c=$(DATABASE_CREATE_TEST) ## Database Test Setup
test-setup-db: sf

test-mm: c=$(MIGRATIONS_MIGRATE_TEST) ## Execute Doctrine migrations for tests
test-mm: sf

test: ## Launch phpunit
	@$(eval c ?=)
	@$(PHPUNIT) $(c)
