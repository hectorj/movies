<?php
declare(strict_types=1);

namespace App\Tests\DrivenAdapter;

use App\Domain\Ports\MovieProvider;
use App\Domain\Ports\MovieRepository;
use App\Infrastructure\DrivenAdapter\Storage\DbalMovieProvider;
use App\Infrastructure\DrivenAdapter\Storage\DbalMovieRepository;

final class DbalMovieStorageTest extends MovieStorageTest
{
    protected function getMovieRepository(): MovieRepository
    {
        return new DbalMovieRepository($this->connection, $this->logger);
    }

    protected function getMovieProvider(): MovieProvider
    {
        return new DbalMovieProvider($this->connection, $this->logger);
    }

    protected function insertPeopleAndTypeForMovie(int $movieId, int $numberOfPeople, int $numberOfTypes): void
    {
        $nationalities = ['anglaise', 'japonaise', 'marocaine', 'péruvienne', 'iranienne'];
        $role = ['actrice', 'réalisatrice', 'productrice'];
        $significance = ['principal', 'secondaire', null];

        for ($i = 1; $i < $numberOfPeople; $i++) {
            $this->connection->insert('people', [
                'id' => $i,
                'firstname' => "firstname$i",
                'lastname' => "lastname$i",
                'date_of_birth' => "1985-08-$i",
                'nationality' => $nationalities[rand(0, 4)],
            ]);
            $this->connection->insert('movie_has_people', [
                'Movie_id' => $movieId,
                'People_id' => $i,
                'role' => $role[rand(0, 2)],
                'significance' => $significance[rand(0, 2)],
            ]);
        }

        $types = ['fantastique', 'horreur', 'science-fiction', 'policier'];

        for ($i = 0; $i < $numberOfTypes; $i++) {
            $id = $i+1;
            $this->connection->insert('type', [
                'id' => $id,
                'name' => $types[$i],
            ]);
            $this->connection->insert('movie_has_type', [
                'Movie_id' => $movieId,
                'Type_id' => $id,
            ]);
        }
    }
}
